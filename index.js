let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log(trainer.pokemon[0] +"! I choose you!");
    }
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();


function Pokemon(name, level){
    // properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    // methods
    this.tackle = function(target){

        console.log(this.name + ' tackled ' + target.name);
        let reducedHealth =  Number(target.health - this.attack);

        if(reducedHealth > 0){
            console.log(target.name + "'s health is now reduced to " + reducedHealth);
            target.health =  reducedHealth;
        }
        else if(reducedHealth <= 0){

            console.log(target.name + "'s health is now reduced to " + reducedHealth );
            // this.faint = function(){
                target.health =  reducedHealth;
                console.log(target.name + " fainted.")
            // }
        }
    }
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);